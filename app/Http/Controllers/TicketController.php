<?php

namespace App\Http\Controllers;
use File;
use Illuminate\Http\Request;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class TicketController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function Get(Request $request){
        $bucket = 'backend-tickets';
        $keyname = $request->input('path');
        #$keyname = "00/00/03/00/00030000.gif";

        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-central-1'
        ]);

        try {
            $result = $s3->getObject([
                'Bucket' => $bucket,
                'Key'    => $keyname
            ]);
            return response($result['Body'])->header("Content-Type","image");;

        } 
        catch (S3Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }

    }

    public function Upload(Request $request){

        $request->file('ticket')->move("/tmp",$request->file('ticket')->getClientOriginalName());     
        $s3 = new S3Client([
            'region'  => 'eu-central-1',
            'version' => 'latest'
        ]);

        try {
            $result = $s3->putObject([
                'Bucket' => 'backend-tickets',
                'Key'    => $request->file('ticket')->getClientOriginalName(),
                'SourceFile' => "/tmp/" . $request->file('ticket')->getClientOriginalName()
            ]);
            echo $result['ObjectURL'] . PHP_EOL;
            unlink("/tmp/" . $request->file('ticket')->getClientOriginalName());
        }
        catch (S3Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }

    }
}
